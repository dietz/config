##################################################
# How to setup my OS from Fedora minimal install #
##################################################

#######################
# Before installation #
#######################

Files needed :
    - OS_setup.txt
    - Private key
    - Owner trust
    - Thunderbird profile
    - Agenda

################
# Installation #
################

- setup gpg
    - gpg --import privkey.asc
    - gpg --import-ownertrust otrust.txt
- install git
- setup dotfiles
    - git clone git@gitlab.gnugen.ch:dietz/dotfiles.git

- setup pass
    - gpg --decrypt password-store-backup.tar.gz.gpg > password-store-backup.tar.gz
    - tar -xzf password-store-backup.tar.gz (install tar)

- sway

- install i3
- install oh-my-zsh
    - install user util-linux-user
    - sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    - stow

- install Firefox
    - se connecter à Sync (listerdiii@gmail.com)

- setup Thunderbird
    - install Import Export Tools
    - import profile

- setup PulseAudio
    - install pulseaudio and pavucontrol

- setup MPD
    - su -c 'dnf install http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm'
    - install MPD
    - systemctl --user enable mpd
    - stow

- install ncmpcpp

- install Telegram
- install ranger
- install Inconsolata
- install NeoBundle
    - curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh > install.sh
    - sh ./install.sh
- install MPV
- install lxappearance
- install passmenu
- install pinentry-gtk (entry dialogs for passmenu)

- setup NETWORK MANAGER
    - still having trouble with that

- deactivate firewalld as it breaks Xorg and make the laptop shutdown in more
  than 3 minutes

- install acpid (and enable it in systemd), light, pulseaudio-utils, perl-Proc-Daemon and perl-Switch as they are required for the
  acpi-handler
