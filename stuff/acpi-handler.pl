#!/usr/bin/env perl
#
# This script listen to ACPI events and acts accordingly.
# Written by fnux. Adapted by andrea.

use warnings;
use strict;

use IO::Socket::UNIX;
use Switch;

my $ACPID_SOCK = "/var/run/acpid.socket";

sub print_help() {
  print "Usage : ./acpi-handler.pl [opts]
  Options :
  * -h : help message
  * -d : daemonize\n";
}

sub daemonize() {
  use Proc::Daemon;
  Proc::Daemon::Init;
  main();
}

sub main() {
  my $stream = IO::Socket::UNIX->new(
    Type => SOCK_STREAM(),
    Peer => $ACPID_SOCK,
  );

  my $line;
  while ($line = <$stream>) {
    my ($type, $code) = ($line =~ m/^(\S*) (\S*)/);

    print "New event : $code. \n";
    switch($code) {
      case "MUTE"   { system "pactl set-sink-mute alsa_output.pci-0000_00_1f.3.analog-stereo toggle" }
      case "VOLDN"  { system "pactl set-sink-volume alsa_output.pci-0000_00_1f.3.analog-stereo -2%" }
      case "VOLUP"  { system "pactl set-sink-volume alsa_output.pci-0000_00_1f.3.analog-stereo +2%" }
      case "BRTDN"  { system "light -U 2" }
      case "BRTUP"  { system "light -A 2" }
      case "CDPREV" { system "mpc prev" }
      case "CDPLAY" { system "mpc toggle" }
      case "CDNEXT" { system "mpc next" }
      # FIXME: should handle plugged in and out as separate messages
      # case "ACPI0003:00" { system "notify-send -u critical 'Power Management' 'AC ADAPTER toggle' &> /dev/null"}
      else  {
        print "! -> Not handled. \n"
      }
    }
  }
}

my $arg = shift(@ARGV);
switch($arg) {
  case "-h"  { print_help() }
  case "-d"  { daemonize() }
  case undef { main() }
  else {
    print_help()
  }
}
