#!/bin/bash

# Global variables:
DMENU='dmenu -l -i'

CHFR="setxkbmap ch fr"
BEPO="setxkbmap fr bepo"

option1='chfr'
option2='bepo'
ret='\n'
CHOICES=$option1$ret$option2

# Show list of options
choice=$(echo -e ${CHOICES} | $DMENU -p "Keyboard:")

if [ "$choice" = $option1 ]; then
  eval $CHFR
elif [ "$choice" = $option2 ]; then
  eval $BEPO
fi
