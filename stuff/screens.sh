#!/bin/bash

# Global variables:
DMENU='dmenu -l -i'

DOUBLE="xrandr --output eDP-1 --primary --mode 1920x1080 --pos 320x1440
--rotate normal --output DP-1 --mode 2560x1440 --pos 0x0 --rotate normal
--output DP2 --off --output HDMI1 --off --output HDMI2 --off --output VIRTUAL1 --off"
DOUBLE_BACKGROUND="feh --bg-scale ~/Downloads/mesh.png  ~/Downloads/mesh2.png"

SINGLE_DOWN="xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0
--rotate normal --output DP-1 --off --output DP2 --off --output HDMI1 --off
--output HDMI2 --off --output VIRTUAL1 --off"

SINGLE_UP="xrandr --output eDP-1 --off --output DP-1 --mode 2560x1440 --pos 0x0
--rotate normal --output DP2 --off --output HDMI1 --off --output HDMI2 --off
--output VIRTUAL1 --off"

SINGLE_BACKGROUND="feh --bg-scale ~/Downloads/mesh.png"

option1='double'
option2='single_down'
option3='single_up'
ret='\n'
CHOICES=$option1$ret$option2$ret$option3

# Show list of options
choice=$(echo -e ${CHOICES} | $DMENU -p "Number of screens:")

if [ "$choice" = $option1 ]; then
  eval $DOUBLE
  eval $DOUBLE_BACKGROUND
elif [ "$choice" = $option2 ]; then
  eval $SINGLE_DOWN
  eval $SINGLE_BACKGROUND
elif [ "$choice" = $option3 ]; then
  eval $SINGLE_UP
  eval $SINGLE_BACKGROUND
fi
