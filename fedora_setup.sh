#!/bin/sh
# Script dedicated to speeding the config process of a minimal Fedora install

echo "Step 1.1 upgrade the system :"
sudo dnf upgrade

echo "Step 1.2 get the dotfiles :"
sudo dnf install git
git clone git@gitlab.gnugen.ch:dietz/config.git

echo "Step 1.3 enable rmp fusion packages :"
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

echo "Step 2.1 install the essentials :"
sudo dnf install zsh stow gpg pass sway util-linux-user tar rxvt-unicode

echo "Step 2.2 install other necessities :"
sudo dnf install firefox thunderbird pulseaudio passmenu pinentry-gtk polybar
ranger telegram-desktop Inconsolata mpd mpv mpc light pulseaudio-utils perl-Proc-Daemon perl-Switch

echo "Step 3 create sym-links :"
# the script assumes you are in the home folder
stow config/dotfiles

echo "Step 4 miscellaneous :"
# mpd
sudo systemctl --user enable mpd
# vim Neobundle
curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh > install.sh
sh ./install.sh

# Others :

# setup gpg and pass
  # gpg --import privkey.asc
  # gpg --import-ownertrust otrust.txt
  #
  # gpg --decrypt password-store-backup.tar.gz.gpg > password-store-backup.tar.gz
  # tar -xzf password-store-backup.tar.gz (install tar)
# oh-my-zsh

# firefox : Sync
  # manually connect to the account
# thunderbird : import profile, filters
  # do not forget to export them
