# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/home/andrea/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="ys"

export EDITOR=/usr/bin/gvim
export VISUAL=/usr/bin/gvim

# Plugins
plugins=(virtualenvwrapper z history-substring-search)

source $ZSH/oh-my-zsh.sh

# Alias
alias hibernate="systemctl hibernate"
alias suspend="systemctl suspend"
alias sandisk="sudo mount /dev/mmcblk0 /home/andrea/mount/sandisk -v"
alias usandisk="sudo umount /home/andrea/mount/sandisk -v"
alias elements="sudo mount /dev/sda1 /home/andrea/mount/elements -v"
alias uelements="sudo umount /home/andrea/mount/elements -v"
# Git
alias ga="git add --verbose"
alias grm="git rm"
alias gcm="git commit -m"
alias gst="git status"
alias gps="git push"
alias gpl="git pull"
alias gdif="git diff"
alias gf="git fetch"
alias gc="git checkout"
alias gcb="git checkout -b"
alias grt="git restore"
# Vim
alias v="vim"
alias gv="gvim"
