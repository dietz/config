hi clear
syntax reset
let g:colors_name = "mycolors"
set background=dark
set t_Co=256
hi Normal guifg=#d38773 ctermbg=NONE guibg=#252525 gui=NONE

hi DiffText guifg=#b4543f guibg=NONE
hi ErrorMsg guifg=#b4543f guibg=NONE
hi WarningMsg guifg=#b4543f guibg=NONE
hi PreProc guifg=#b4543f guibg=NONE
hi Exception guifg=#b4543f guibg=NONE
hi Error guifg=#b4543f guibg=NONE
hi DiffDelete guifg=#b4543f guibg=NONE
hi GitGutterDelete guifg=#b4543f guibg=NONE
hi GitGutterChangeDelete guifg=#b4543f guibg=NONE
hi cssIdentifier guifg=#b4543f guibg=NONE
hi cssImportant guifg=#b4543f guibg=NONE
hi Type guifg=#b4543f guibg=NONE
hi Identifier guifg=#b4543f guibg=NONE
hi PMenuSel guifg=#7d8876 guibg=NONE
hi Constant guifg=#7d8876 guibg=NONE
hi Repeat guifg=#7d8876 guibg=NONE
hi DiffAdd guifg=#7d8876 guibg=NONE
hi GitGutterAdd guifg=#7d8876 guibg=NONE
hi cssIncludeKeyword guifg=#7d8876 guibg=NONE
hi Keyword guifg=#7d8876 guibg=NONE
hi IncSearch guifg=#b2893f guibg=NONE
hi Title guifg=#b2893f guibg=NONE
hi PreCondit guifg=#b2893f guibg=NONE
hi Debug guifg=#b2893f guibg=NONE
hi SpecialChar guifg=#b2893f guibg=NONE
hi Conditional guifg=#b2893f guibg=NONE
hi Todo guifg=#b2893f guibg=NONE
hi Special guifg=#b2893f guibg=NONE
hi Label guifg=#b2893f guibg=NONE
hi Delimiter guifg=#b2893f guibg=NONE
hi Number guifg=#b2893f guibg=NONE
hi CursorLineNR guifg=#b2893f guibg=NONE
hi Define guifg=#b2893f guibg=NONE
hi MoreMsg guifg=#b2893f guibg=NONE
hi Tag guifg=#b2893f guibg=NONE
hi String guifg=#b2893f guibg=NONE
hi MatchParen guifg=#b2893f guibg=NONE
hi Macro guifg=#b2893f guibg=NONE
hi DiffChange guifg=#b2893f guibg=NONE
hi GitGutterChange guifg=#b2893f guibg=NONE
hi cssColor guifg=#b2893f guibg=NONE
hi Function guifg=#7e5276 guibg=NONE
hi Directory guifg=#b45274 guibg=NONE
hi markdownLinkText guifg=#b45274 guibg=NONE
hi javaScriptBoolean guifg=#b45274 guibg=NONE
hi Include guifg=#b45274 guibg=NONE
hi Storage guifg=#b45274 guibg=NONE
hi cssClassName guifg=#b45274 guibg=NONE
hi cssClassNameDot guifg=#b45274 guibg=NONE
hi Statement guifg=#7d8941 guibg=NONE
hi Operator guifg=#7d8941 guibg=NONE
hi cssAttr guifg=#7d8941 guibg=NONE


hi Pmenu guifg=#d38773 guibg=#383838
hi SignColumn guibg=#252525
hi Title guifg=#d38773
hi LineNr guifg=#999999 guibg=#252525
hi NonText guifg=#baa48a guibg=#252525
hi Comment guifg=#baa48a gui=italic
hi SpecialComment guifg=#baa48a gui=italic guibg=#252525
hi CursorLine guibg=#383838
hi TabLineFill gui=NONE guibg=#383838
hi TabLine guifg=#5d2a1d guibg=#383838 gui=NONE
hi StatusLine gui=bold guibg=#383838 guifg=#d38773
hi StatusLineNC gui=NONE guibg=#252525 guifg=#d38773
hi Search guibg=#2b4057 guifg=#007791
hi VertSplit gui=NONE guifg=#383838 guibg=NONE
hi Visual gui=NONE guibg=#383838

hi Folded gui=NONE guibg=#383838
hi FoldColumn gui=NONE guibg=#383838
