#!/bin/zsh

cd /home/andrea/config/dotfiles/.config/i3/
CONF=config

touch $CONF
cat i3_misc >$CONF
cat i3_applications >>$CONF
cat i3_workspaces >>$CONF
