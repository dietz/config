" NeoBundle Scripts-------------------------------------------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=/home/andrea/.vim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('/home/andrea/.vim/bundle'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:

" auto complete
"NeoBundle 'Shougo/neocomplete.vim'
" ?
"NeoBundle 'sheerun/vim-polyglot'
" ?
NeoBundle 'vim-airline/vim-airline'
NeoBundle 'vim-airline/vim-airline-themes'
"
NeoBundle 'scrooloose/nerdtree'
" python-mode: PyLint, Rope, Pydoc, breakpoints from box.
" https://github.com/python-mode/python-mode
"NeoBundleLazy 'python-mode/python-mode', { 'on_ft': 'python' }

" Required:
call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck
"End NeoBundle Script----------------------------------------------------------

"Plugins config----------------------------------------------------------------

" NeoComplete
let g:neocomplete#enable_at_startup = 1

" Vim-airline : display status bar
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='angr'

" NerdTree
map <Enter> :NERDTreeToggle<CR>


"Look--------------------------------------------------------------------------

" Colorscheme
colorscheme mycolors

" Use terminal background
hi Normal     ctermbg=NONE guibg=NONE
hi LineNr     ctermbg=NONE guibg=NONE
hi SignColumn ctermbg=NONE guibg=NONE

" Display and format line numbers.
set number
set numberwidth=4

" Enable UTF-8.
set encoding=utf8

" syntax highlighting
syntax on
set hlsearch

"Keybindings-------------------------------------------------------------------

" Neosnippet key-mappings.
"imap <C-k>     <Plug>(neosnippet_expand_or_jump)
"smap <C-k>     <Plug>(neosnippet_expand_or_jump)
"xmap <C-k>     <Plug>(neosnippet_expand_target)

" Neosnippet superTab like snippets behavior.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
"smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
"\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"


"Misc--------------------------------------------------------------------------

" Mouse support ("all")
"set mouse=a"

" Show command and mode.
set showcmd

" Make backspace work like most other apps.
set backspace=2

" Auto indentation.
set autoindent
set smartindent
set copyindent

" Auto change directory
set autochdir

" Save the undo tree
if ! isdirectory($HOME . "/.vim/undo")
  call mkdir($HOME . "/.vim/undo", "p")
endif
set undofile
" Save it in ~/.vim/undo/.
set undodir=$HOME/.vim/undo

" Use spaces instead of tabs.
set expandtab
set tabstop=2
set shiftwidth=2

filetype plugin on
set grepprg=grep\ -nH\ $*
filetype indent on
let g:tex_flavor='latex'

" Max number of characters per line (0 for no restriction)
set tw=79

" Coding is a pain without syntax higlighting
syntax on

"Lets put an end to the invasion of .swp in home
set swapfile
set dir=~/.vim
"Lets put backup files in .vim
set backup
set backupdir=~/.vim

" Put viminfo file in .vim
set viminfo+=n~/.vim/viminfo

"Display spaces and some stuff
set list
set listchars=nbsp:_,trail:~,extends:>,precedes:<,tab:>-

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

"Bindings---------------------------------------------------------------------

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file (here it is <space>)
let mapleader = " "

" Moving around buffers
map <leader>l :bnext<cr>
map <leader>h :bprevious<cr>

" Opens a new tab with the current buffer's path
" Super useful when editing files in the same directory
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" Save using sudo
cmap w!! w !sudo tee % >/dev/null

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

"Bindings---------------------------------------------------------------------

" Remap VIM 0 to first non-blank character
map 0 ^

" Delete trailing white space on save, useful for some filetypes ;)
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre * :call CleanExtraSpaces()
endif

"folding settings ; use 'za' (toggle)
"FoldAll 'zM' ; UnfolfAll 'zR'
set foldmethod=indent   "fold based on syntax
"set foldnestmax=10      "deepest fold is 10 levels
set nofoldenable        "dont fold by default
set foldlevel=1         "this is just what i use

"remove unecessary graphical user interfaces
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
set guioptions-=F  "remove message footer
set guioptions-=b  "remove horizontal scroll bar
set guioptions+=p  "can solve cursor display problem


set guifont=Inconsolata\ 15

" use Xclipboard
set clipboard=unnamedplus

" guifont size in normal mode !
nnoremap + :silent! let &guifont = substitute(
 \ &guifont,
 \ ' \zs\d\+',
 \ '\=eval(submatch(0)+1)',
 \ '')<CR>
nnoremap - :silent! let &guifont = substitute(
 \ &guifont,
 \ ' \zs\d\+',
 \ '\=eval(submatch(0)-1)',
 \ '')<CR>

" Random cursor config
highlight iCursor guifg=white guibg=steelblue
set guicursor=n-v-c:block-Cursor
set guicursor+=i:ver100-iCursor
set guicursor+=n-v-c:blinkon0
set guicursor+=i:blinkon1000

" autocomplete ?
"""" ^X to enter
"""" ^P previous, ^N next
"""" ^E reject, <ESC> to leave (or type)
"""" ^Xs for spelling correction
filetype plugin on
set spell
set omnifunc=syntaxcomplete#Complete
